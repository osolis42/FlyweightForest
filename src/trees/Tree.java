package trees;

import java.awt.*;

public class Tree {
    private int x;
    private int y;
    private TreeType type;

    public Tree(int x, int y, TreeType type) { // Coordenadas
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public void draw(Graphics g) { // Referencia de posicion
        type.draw(g, x, y);
    }
}
