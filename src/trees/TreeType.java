package trees;

import java.awt.*;

public class TreeType {
    private String name;
    private Color color;
    private String otherTreeData;

    public TreeType(String name, Color color) { // Valores unicos compartidos de los árboles
        this.name = name;
        this.color = color;
    }

    public void draw(Graphics g, int x, int y) { // Posición de las figuras que forman al árbol.
        g.setColor(Color.BLACK);
        g.fillRect(x - 1, y, 3, 5);
        g.setColor(color);
        g.fillOval(x - 5, y - 10, 10, 10);
    }
}